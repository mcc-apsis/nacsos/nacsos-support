# %%
import logging
import uuid

import pandas as pd
import numpy as np
from sqlalchemy import text

from nacsos_data.db import get_engine

db_engine = get_engine(conf_file='../.config/remote.env')

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('import')
logger.setLevel(logging.DEBUG)

pd.options.display.max_columns = None

PROJECT_ID = 'ce9a954f-a3c3-4784-b732-19f3fda31e2e'

MULTI_FILTERS = [
    (name, f"AND an.key='{key}' AND an.multi_int && ARRAY[{value}]")
    for key, values in [('meth', [('Techno-economic (TEA) or life cycle assessment (LCA)', 0),
                                  ('Modelling (process- or system-level) ', 1),
                                  ('Experiment (field study or laboratory) ', 3),
                                  ('Review or systematic review ', 6),
                                  ('Biomass feedstock and supply assessment ', 9),
                                  ('Surveys, interviews, document analysis, observations ', 8),
                                  ('Monitoring, Reporting and Verification (MRV) ', 7)]),
                        ('focus', [('Public perception & acceptance', 0),
                                   ('Policy, politics & governance', 1),
                                   ('Socio-economic pathways', 3),
                                   ('Technology & scale-up', 4), ])]
    for name, value in values
]
SINGLE_FILTERS = [
    ('Predominantly on BECCS', "AND an.key='tech' AND an.value_int=0"),
    ('Mitigation & general CDR (incl BECCS)', "AND an.key='tech' AND an.value_int=1"),
    ('Mitigation & general CDR (incl BECCS)', "AND an.key='tech' AND an.value_int=2")
]

with pd.ExcelWriter('data/authors.xlsx', engine='xlsxwriter') as writer:
    for name, query in [('Unfiltered', '')] + SINGLE_FILTERS + MULTI_FILTERS:
        stmt = text(f'''
            WITH
                authors_raw as (
                    SELECT jsonb_array_elements(ai.authors) author
                    FROM academic_item ai
                    JOIN annotation an ON ai.item_id = an.item_id
                    WHERE ai.project_id = :project_id
                    {query}
                        -- AND an.key='tech' AND an.value_int=0
                        -- AND an.key='meth' AND an.multi_int && ARRAY[0,1,3,5,4]
                    ),
                authors as (
                    SELECT a.author ->> 'name' name, a.author ->> 'orcid' orcid, a.author ->> 'openalex_id' openalex_id
                    FROM authors_raw a)
            SELECT COUNT(1) as n_occurrences, a.*
            FROM authors a
            GROUP BY a.name, a.orcid, a.openalex_id
            ORDER BY n_occurrences DESC
            LIMIT 200;
        ''')
        with db_engine.session() as session:
            df = pd.DataFrame(session.execute(stmt, {'project_id': PROJECT_ID}).mappings().all())
            df.to_excel(writer, sheet_name=name[:31], index=False)
