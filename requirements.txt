nacsos_data[utils,scripts] @ git+ssh://git@gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-data.git@v0.20.8
pandas==2.2.3
openpyxl==3.2.0b1
pyarrow==19.0.0
matplotlib==3.10.0
ipykernel==7.0.0a0
torch==2.6.0
evaluate==0.4.3
tqdm==4.67.1
datasets==3.2.0
transformers==4.48.2
grobid_client==0.8.8
grobid_tei_xml==0.1.3
krippendorff==0.8.1
xlsxwriter==3.2.2