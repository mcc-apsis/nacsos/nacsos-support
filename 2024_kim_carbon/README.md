# Carbon pricing review ecosystem
Lead: Mi Lim Kim (Crawford School of Public Policy Australian National University [ANU] College of Asia & the Pacific)   
Team: Niklas DH, Klaas, Tim, Stephan Bruns, David Stern, Banna Banik

Based on
```
(({!surround v="(carbon OR emission? OR CO2 OR GHG) 2N (price? OR pricing OR tax OR taxes OR taxation OR trading OR trade OR tradable OR levy OR levies OR allowance? OR market)"} OR "cap-and-trade" OR "climate-change-levy")
AND ({!surround v="(carbon OR emission OR emissions OR co2 OR ghg OR greenhouse) 2N (intensit?)"})
AND (did OR "difference-in-difference" OR "difference-in-differences" OR "differences-in-difference" OR "differences-in-differences" OR "synthetic-control" OR "quasi-experiment" OR "quasi-experiments" OR "quasi-experimental" OR "quasi-natural" OR "natural-experiment" OR "natural-experiments" OR "natural-experimental" OR matching OR panel OR ( {!surround v="(regression 2N discontinuit?)"} ) OR ( {!surround v="(instrumental 2N variable?)"} ) OR ( {!surround v="(fixed 2N effect?)"} ))
)
```
