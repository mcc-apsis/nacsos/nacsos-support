import asyncio
import logging
import uuid

from nacsos_data.db.schemas import ItemType
from nacsos_data.models.items import AcademicItemModel
from nacsos_data.models.nql import SubQuery, LabelFilterMulti, LabelFilterInt
from nacsos_data.util.academic.importer import import_academic_items
from nacsos_data.util.nql import NQLQuery
from sqlalchemy import func
from nacsos_data.db import get_engine_async, DatabaseEngineAsync, get_engine
from nacsos_data.db.schemas.imports import Import
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Session

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('import')
logger.setLevel(logging.DEBUG)

BATCH_SIZE = 100

PROJECT_SRC = '748e739d-f011-44de-9cb0-c9cb4bb18d08'
PROJECT_TGT = '5b426375-55e1-4065-8fad-5fe7276d81c0'

# QUERY = '{"filter":"sub","and_":[{"filter":"label_multi","type":"resolved","value_type":"multi","key":"outc","multi_int":[0],"comp":"&&","users":null},{"filter":"label_int","type":"resolved","value_type":"int","key":"meth","value_int":3,"comp":"<","users":null}]}'
QUERY = SubQuery(
    filter='sub',
    and_=[
        LabelFilterMulti(key='outc', type='resolved',
                         filter='label_multi', value_type='multi', multi_int=[0], comp='&&'),
        LabelFilterInt(key='meth', type='resolved',
                       filter='label_int', value_type='int', value_int=3, comp='<')
    ],
    or_=None,
    not_=None)


def items(db_engine: DatabaseEngineAsync):
    async def inner():
        async with db_engine.session() as session:  # type: AsyncSession
            nql = NQLQuery(QUERY, project_id=PROJECT_SRC, project_type=ItemType.academic)
            n_docs = (await session.execute(func.count(nql.stmt.subquery().c.item_id))).scalar()
            logger.info(f'Found {n_docs} items for the given query in the project {PROJECT_SRC}')

            rslt = (await session.stream(nql.stmt.execution_options(yield_per=BATCH_SIZE))).mappings().partitions()
            async for bi, batch in enumerate(rslt):
                logger.debug(f'Received batch {bi}')
                for item in batch:
                    doc = AcademicItemModel.model_validate(item['AcademicItem'].__dict__)
                    doc.item_id = uuid.uuid4()
                    doc.project_id = PROJECT_TGT
                    yield item
    return inner


async def main():
    db_engine = get_engine_async(conf_file='../.config/remote.env')
    db_engine_s = get_engine(conf_file='../.config/remote.env')

    def items():
        with db_engine_s.session() as session:  # type: Session
            nql = NQLQuery(QUERY, project_id=PROJECT_SRC, project_type=ItemType.academic)
            n_docs = session.execute(func.count(nql.stmt.subquery().c.item_id)).scalar()
            logger.info(f'Found {n_docs} items for the given query in the project {PROJECT_SRC}')

            rslt = session.execute(nql.stmt.execution_options(yield_per=BATCH_SIZE)).mappings().partitions()
            for bi, batch in enumerate(rslt):
                logger.debug(f'Received batch {bi}')
                for item in batch:
                    doc = AcademicItemModel.model_validate(item['AcademicItem'].__dict__)
                    doc.item_id = uuid.uuid4()
                    doc.project_id = PROJECT_TGT
                    yield doc

    async with db_engine.session() as session:  # type: AsyncSession

        import_id=str(uuid.uuid4())
        imp = Import(
            type='script',
            project_id=PROJECT_TGT,
            import_id=import_id,
            user_id='fd641232-bada-466e-9a3b-fb12038f5508',
            name='Filtered documents from carbon pricing map',
            description='''Abstracts that were manually annotated (in resolution) as

"Query": 
  (“environmental effectiveness” AND (“quasi-experiment” OR “statistical inference” OR “other quantitative”))

NQL: 
  LABEL:RES outc && [0] AND LABEL:RES meth<3''',
        )
        session.add(imp)
        await session.commit()
        logger.info(f'Created import with ID={import_id}')

    await import_academic_items(
        db_engine=db_engine,
        project_id=PROJECT_TGT,
        new_items=items,
        import_id=import_id,
        vectoriser=None,
        max_slop=0.05,
        batch_size=5000,
        dry_run=False,
        trust_new_authors=False,
        trust_new_keywords=False,
        logger=logger
    )

if __name__ == '__main__':
    asyncio.run(main())
