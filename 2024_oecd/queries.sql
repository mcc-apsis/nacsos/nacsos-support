DELETE
FROM assignment
WHERE assignment_id IN (WITH counts AS (SELECT ass.assignment_id, count(ann.assignment_id) as cnt
                                        FROM assignment ass
                                                 JOIN assignment_scope scope
                                                      ON ass.assignment_scope_id = scope.assignment_scope_id
                                                 JOIN annotation_scheme scheme
                                                      ON scheme.annotation_scheme_id = scope.annotation_scheme_id
                                                 LEFT OUTER JOIN annotation ann ON ass.assignment_id = ann.assignment_id
                                        WHERE scheme.project_id = '8d36c360-3775-4d43-8f30-9fcd6151fe30'
                                        GROUP BY ass.assignment_id)
                        SELECT assignment_id
                        FROM counts
                        WHERE cnt = 0);