import httpx
import pandas as pd
from pathlib import Path
from grobid_client.grobid_client import GrobidClient

import logging

BASE = Path('data/')

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s',
                    level=logging.INFO, filename=BASE / 'pdfs.log')
logger = logging.getLogger('notebook')
logger.setLevel(logging.DEBUG)

FILES = [
    (BASE / 'pdfs', BASE / 'teis'),
]

client = GrobidClient(config_path='grobid.config.json')
for src, tgt in FILES:
    logger.info(src)
    logger.info(tgt)
    client.process(
        service='processHeaderDocument',  # 'processFulltextDocument',
        input_path=src,  # input directory
        output=tgt,  # output directory
        n=10,  # thread pool size
        segment_sentences=True,
        verbose=True
    )
