# This script assigns screening labels from the 166 PDFs
#   -> of which 76 are matched to OpenAlex IDs
#   -> of which 60 are in the query (50 after dedup)
# to the imported items on the platform.

import asyncio
import logging
import uuid

import pandas as pd
from sqlalchemy import text
from nacsos_data.db import get_engine_async
from nacsos_data.db.schemas.bot_annotations import BotAnnotation

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('import')
logger.setLevel(logging.DEBUG)

PROJECT_ID = 'fe0fd6ae-18d8-4260-af82-6aaba8c85f87'
SCOPE_ID = '2fbc7d69-2f7a-4512-8b3e-55a3ce25c56c'
RESOL_ID = '9bf07228-9745-4c5e-895c-5450527e5a22'

async def main():
    db_engine = get_engine_async(conf_file='../.config/remote.env')
    df = pd.read_excel('data/included_papers_AM.xlsx')

    async with db_engine.session() as session:
        oa_ids = [row['OpenAlex'].upper() for _, row in df.iterrows() if row['OpenAlex'].lower().startswith('w')]
        stmt = text('''
            SELECT item_id::text 
            FROM academic_item 
            WHERE openalex_id = ANY(:oaid) AND project_id = :pid
            UNION
            SELECT ai.item_id::text 
            FROM academic_item_variant aiv 
            JOIN academic_item ai ON aiv.item_id = ai.item_id
            WHERE aiv.openalex_id = ANY(:oaid) AND ai.project_id = :pid
        ''')
        res = await session.execute(stmt, {'oaid': oa_ids, 'pid': PROJECT_ID})
        rslt = res.mappings().all()
        print(len(rslt))
        for r in rslt:
            session.add(BotAnnotation(
                bot_annotation_id=uuid.uuid4(),
                bot_annotation_metadata_id=RESOL_ID,
                item_id=r['item_id'],
                key='incl',
                repeat=1,
                value_bool=True,
            ))

        await session.commit()

if __name__ == '__main__':
    asyncio.run(main())
