import json
from pathlib import Path
import pandas as pd
import grobid_tei_xml

acc = []
for fname in Path('data/teis').glob('*.xml'):
    basename =fname.stem.split('.')[0]

    with open(fname, 'r') as xml_file:
        txt = xml_file.read()
        doc = grobid_tei_xml.parse_document_xml(txt)

    #print(json.dumps(doc.to_dict(), indent=2))
    doc = grobid_tei_xml.parse_document_xml(txt)

    acc.append({
        'basename': basename,
        'title': doc.header.title,
        'py': doc.header.date,
        'doi': str(doc.header.doi),
        'journal': doc.header.journal,
        'abstract': doc.abstract,
        'authors': '||'.join([a.full_name for a in doc.header.authors]),
        'n_cit': str(len(doc.citations)),
    })

pd.DataFrame(acc).to_excel('data/included_papers.xlsx', index=False)
