from transformers import AutoTokenizer, AutoModelForSequenceClassification
import pandas as pd
import torch
import os


# Function to make predictions for a specific target
def make_predictions(input_data, target):
    # Specify the path to the best model and tokenizer directories
    model_dir = f"./best_model_{target}"

    # Load the pretrained tokenizer and model
    tokenizer = AutoTokenizer.from_pretrained(model_dir)
    model = AutoModelForSequenceClassification.from_pretrained(model_dir)

    # Preprocess the input data
    input_texts = input_data["Abstract"].dropna().tolist()
    input_encodings = tokenizer(input_texts, padding="max_length", truncation=True, max_length=512, return_tensors="pt")

    # Use the model for inference
    with torch.no_grad():
        outputs = model(**input_encodings)
        predicted_labels = torch.argmax(outputs.logits, dim=1)

    # Add predictions to the input data
    input_data[f"{target}_pred"] = None
    input_data.loc[input_data["Abstract"].notna(), f"{target}_pred"] = predicted_labels.numpy()

    print(f"Prediction of {target} complete")

    return input_data


# Set the working directory
os.chdir("C:/Users/sm20s840/Documents/barriers_and_enablers")

# Load the input data
input_data = pd.read_excel(r'data/all_articles_new_search_string_clean.xlsx')

# input_data = input_data.iloc[400:420]

# Specify the list of targets
targets = ["feasibility", "mitigation", "energy", "transport", "buildings", "industry", "afolu"]

# Make predictions for each target
for target in targets:
    input_data = make_predictions(input_data.copy(), target)

# Save the predictions to a new Excel file
input_data.to_excel("data/predicted_articles.xlsx", index=False)
