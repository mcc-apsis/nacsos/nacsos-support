import os
import pandas as pd
import numpy as np
import torch
from transformers import AutoTokenizer, AutoModelForSequenceClassification, Trainer, logging, TrainingArguments
from datasets import Dataset, load_metric
from sklearn.model_selection import KFold, ParameterGrid
from collections import Counter
import ray
from multiprocessing import cpu_count
from sklearn.utils import resample

# Set the working directory
os.chdir("C:/Users/sm20s840/Documents/barriers_and_enablers")  # os.chdir("/Users/simon/Documents/repo/barriers_and_enablers")
df = pd.read_excel(r'data/sample_SM_train_new_search_string_15.xlsx')

# Subset to annotated rows
df = df.iloc[:1336]  # 1336

# Convert specified columns to numeric
cols = ["economic_cost", "distributional_dynamics", "institutional_capacity", "multilevel"]
df[cols] = df[cols].apply(pd.to_numeric, errors='coerce', axis=1)

# Create a new column 'any_barrier_yes'
df['any_barrier_yes'] = df[cols].any(axis=1).astype(int)

targets = ["buildings"]  # "industry", "waste", "afolu", "any_barrier_yes"

for target in targets:
    df[target] = df[target].fillna(0).astype(int)

# Drop rows with missing values in 'Abstract'
df = df.dropna(subset=["Abstract"])


# Define a function to tokenize the dataset
def datasetify(texts, tokenizer, labels=None, weights=None):
    data_dict = {"text": texts}
    if labels is not None:
        data_dict["label"] = labels
    if weights is not None:
        data_dict["weight"] = weights
    dataset = Dataset.from_dict(data_dict)

    def tokenize_function(examples):
        return tokenizer(examples["text"], padding="max_length", truncation=True, max_length=512)

    return dataset.map(tokenize_function, batched=True)


# Load the pretrained tokenizer
model_name = "climatebert/distilroberta-base-climate-f"
tokenizer = AutoTokenizer.from_pretrained(model_name)

# Initialize logging
logging.set_verbosity_warning()


# Define the compute metrics function
def compute_metrics(eval_pred):
    load_accuracy = load_metric("accuracy", trust_remote_code=True)
    load_f1 = load_metric("f1", trust_remote_code=True)
    load_recall = load_metric("recall", trust_remote_code=True)
    load_precision = load_metric("precision", trust_remote_code=True)

    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    accuracy = load_accuracy.compute(predictions=predictions, references=labels)["accuracy"]
    f1 = load_f1.compute(predictions=predictions, references=labels)["f1"]
    recall = load_recall.compute(predictions=predictions, references=labels)["recall"]
    precision = load_precision.compute(predictions=predictions, references=labels)["precision"]

    return {"accuracy": accuracy, "f1": f1, "recall": recall, "precision": precision}

# Define a function to calculate class weights
def calculate_class_weights(y):
    counter = Counter(y)
    total = sum(counter.values())
    weights = {cls: total / count for cls, count in counter.items()}
    return weights

# Define outer cross-validation
outer_kf = KFold(n_splits=2, shuffle=True, random_state=2)

# Define the parameter grid for hyperparameter tuning
param_grid = {
    'learning_rate': [2e-5, 5e-5, 3e-5],  #
    'per_device_train_batch_size': [8, 16],  #
    'num_train_epochs': [3, 5],  # ,
    'weight_decay': [0.01, 0.1],  # ,
}


# Define a function to perform oversampling
def oversample(df, target):
    # Separate majority and minority classes
    df_majority = df[df[target] == 0]
    df_minority = df[df[target] == 1]

    # Upsample minority class
    df_minority_upsampled = resample(df_minority,
                                     replace=True,  # sample with replacement
                                     n_samples=len(df_majority),  # to match majority class
                                     random_state=123)  # reproducible

    # Combine majority class with upsampled minority class
    df_upsampled = pd.concat([df_majority, df_minority_upsampled])

    return df_upsampled


# Define a function to train and evaluate the model for a specific target
def train_and_evaluate(target):
    print(f"Target variable: {target}")
    best_outer_f1 = 0
    best_model = None
    outer_test_metrics = []  # Define outer_test_metrics here

    # Perform the outer cross-validation
    for outer_fold, (outer_train_index, outer_test_index) in enumerate(outer_kf.split(df), start=1):
        print(f"\nOuter Fold: {outer_fold}")
        outer_train_df, outer_test_df = df.iloc[outer_train_index], df.iloc[outer_test_index]

        # Oversample the training data
        outer_train_df = oversample(outer_train_df, target)

        # Initialize inner cross-validation
        inner_kf = KFold(n_splits=4, shuffle=True, random_state=2)

        best_inner_metrics = None
        best_params = None

        # Perform the inner cross-validation
        for params in ParameterGrid(param_grid):
            print(f"\n{target} -- testing hyperparameters: {params}")
            inner_test_metrics = []

            for inner_fold, (inner_train_index, inner_test_index) in enumerate(inner_kf.split(outer_train_df), start=1):
                print(f"\n{target} -- inner fold: {inner_fold}")
                inner_train_fold_df, inner_test_fold_df = outer_train_df.iloc[inner_train_index], outer_train_df.iloc[
                    inner_test_index]

                # Prepare train and test datasets for inner fold
                inner_train_text = inner_train_fold_df["Abstract"]
                inner_test_text = inner_test_fold_df["Abstract"]
                inner_train_y = inner_train_fold_df[target]
                inner_test_y = inner_test_fold_df[target]

                # Calculate observed case weights for the inner training set
                class_weights = calculate_class_weights(inner_train_y)
                weights = [class_weights[label] for label in inner_train_y]

                inner_train_dataset = datasetify(inner_train_text.tolist(), tokenizer, inner_train_y.tolist(), weights)
                inner_test_dataset = datasetify(inner_test_text.tolist(), tokenizer, inner_test_y.tolist())

                # Load the pretrained model with specified number of labels
                model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=2)

                # Set training arguments
                training_args = TrainingArguments(
                    output_dir="./results",
                    eval_strategy="epoch",
                    save_strategy="epoch",
                    metric_for_best_model="f1",
                    load_best_model_at_end=True,
                    overwrite_output_dir=True,
                    learning_rate=params['learning_rate'],
                    per_device_train_batch_size=params['per_device_train_batch_size'],
                    per_device_eval_batch_size=params['per_device_train_batch_size'],
                    num_train_epochs=params['num_train_epochs'],
                    warmup_steps=500,
                    weight_decay=params['weight_decay'],
                    logging_dir='./logs',
                    logging_steps=10,
                    gradient_accumulation_steps=2,
                )

                # Custom trainer class to handle case weights
                class WeightedTrainer(Trainer):
                    def compute_loss(self, model, inputs, return_outputs=False):
                        labels = inputs.get("labels")
                        weights = inputs.get("weight", None)
                        outputs = model(**inputs)
                        logits = outputs.get("logits")
                        loss_fct = torch.nn.CrossEntropyLoss(
                            weight=torch.tensor(weights) if weights is not None else None)
                        loss = loss_fct(logits, labels)
                        return (loss, outputs) if return_outputs else loss

                # Initialize the Trainer with case weights if provided
                trainer = WeightedTrainer(
                    model=model,
                    args=training_args,
                    train_dataset=inner_train_dataset,
                    eval_dataset=inner_test_dataset,
                    compute_metrics=compute_metrics
                )

                # Train the model
                trainer.train()

                # Evaluate the model on the inner test set
                eval_result = trainer.evaluate(inner_test_dataset)
                inner_test_metrics.append(eval_result)

                print(f"\n{target} -- inner Fold {inner_fold} metrics: {eval_result}")

            # Compute mean metrics for the current set of hyperparameters
            mean_inner_test_metrics = {metric: np.mean([fold_metrics[metric] for fold_metrics in inner_test_metrics])
                                       for metric in inner_test_metrics[0]}

            # Update best parameters based on F1 score
            if best_inner_metrics is None or mean_inner_test_metrics['eval_f1'] > best_inner_metrics['eval_f1']:
                best_inner_metrics = mean_inner_test_metrics
                best_params = params

        print(f"\n{target} -- best hyperparameters: {best_params} with metrics: {best_inner_metrics}")

        # Prepare train and test datasets for outer fold using the best parameters found
        outer_train_text = outer_train_df["Abstract"]
        outer_test_text = outer_test_df["Abstract"]
        outer_train_y = outer_train_df[target]
        outer_test_y = outer_test_df[target]

        # Calculate observed case weights for the outer training set
        class_weights = calculate_class_weights(outer_train_y)
        outer_weights = [class_weights[label] for label in outer_train_y]

        outer_train_dataset = datasetify(outer_train_text.tolist(), tokenizer, outer_train_y.tolist(), outer_weights)
        outer_test_dataset = datasetify(outer_test_text.tolist(), tokenizer, outer_test_y.tolist())

        # Load the model with the best parameters
        model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=2)

        # Set training arguments using best parameters
        training_args = TrainingArguments(
            output_dir=f"./results/{target}",  # Save results for each target separately
            eval_strategy="epoch",
            save_strategy="epoch",
            metric_for_best_model="f1",
            load_best_model_at_end=True,
            overwrite_output_dir=True,
            learning_rate=best_params['learning_rate'],
            per_device_train_batch_size=best_params['per_device_train_batch_size'],
            per_device_eval_batch_size=best_params['per_device_train_batch_size'],
            num_train_epochs=best_params['num_train_epochs'],
            warmup_steps=500,
            weight_decay=best_params['weight_decay'],
            logging_dir='./logs',
            logging_steps=10,
            gradient_accumulation_steps=2,
        )

        # Initialize the Trainer with case weights for the outer fold
        trainer = WeightedTrainer(
            model=model,
            args=training_args,
            train_dataset=outer_train_dataset,
            eval_dataset=outer_test_dataset,
            compute_metrics=compute_metrics
        )

        # Train the model on the outer training set
        trainer.train()

        # Evaluate the model on the outer test set
        outer_eval_result = trainer.evaluate(outer_test_dataset)
        outer_test_metrics.append(outer_eval_result)

        # Export out-of-sample performance on the outer fold
        outer_test_performance = pd.DataFrame(outer_test_metrics)
        outer_test_performance.to_csv(f"outer_test_performance_{target}.csv", index=False)

        print(f"\n{target} -- outer Fold {outer_fold} test metrics: {outer_eval_result}")

        # Save the best model if it improves the F1 score
        if outer_eval_result['eval_f1'] > best_outer_f1:
            best_outer_f1 = outer_eval_result['eval_f1']
            best_model = model

    # Save the best model for each target
    best_model.save_pretrained(f"./best_model_{target}")
    tokenizer.save_pretrained(f"./best_model_{target}")


for target in targets:
    train_and_evaluate(target)