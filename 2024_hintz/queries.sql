-- PROJECT_ID = '3d761435-fe4f-4829-a33e-26fc52a46613'
-- SCHEME_ID = '4658a522-0626-4074-b207-ed46ea13d2a1'
--
--     i for i, _ in [('b386ae78-ae35-428a-be34-abceab9dba3f', 'Resolution for "Waste"'),
--                    ('0057dc42-5821-445b-b3d5-834d0958d110', 'Resolution for "urban form"'),
--                    ('c42b71c7-f1dc-46b2-b187-72fa78f8a397', 'Resolution for "buildings"'),
--                    ('c77b8462-7ab5-4bac-b686-6008c8fbe727', 'Resolution for "transport"'),
--                    ('be1361b6-35ed-4509-bc2a-da10c9d83e91', 'Resolution for "bikes and scooters"')]
-- ]
--
-- with db_engine.session() as session:
--     stmt = (
--         select(AcademicItem, BotAnnotation.key, BotAnnotation.value_int.label('value'))
--         .join(BotAnnotation, and_(BotAnnotation.item_id == AcademicItem.item_id,
--                                   BotAnnotation.bot_annotation_metadata_id.in_(sources),
--                                   BotAnnotation.key.in_(IMPACT_KEYS + ['incl', 'cluster']) ),
--               isouter=True)
--         .where(AcademicItem.project_id == PROJECT_ID)

WITH
    labels_flat as (
        SELECT i.item_id,
               json_build_object(ba.key, ba.value_int) as label
        FROM item i
             JOIN bot_annotation ba ON ba.item_id = i.item_id
        WHERE i.project_id = '3d761435-fe4f-4829-a33e-26fc52a46613'
          AND ba.key = ANY (array [
            'incl','cluster',
            'imp_build', 'imp_trans', 'imp_uf', 'imp_waste', 'imp_meta'
            ])
          AND ba.bot_annotation_metadata_id::text = ANY (array [
            'b386ae78-ae35-428a-be34-abceab9dba3f', -- Resolution for "Waste"
            '0057dc42-5821-445b-b3d5-834d0958d110', -- Resolution for "urban form"
            'c42b71c7-f1dc-46b2-b187-72fa78f8a397', -- Resolution for "buildings"
            'c77b8462-7ab5-4bac-b686-6008c8fbe727', -- Resolution for "transport"
            'be1361b6-35ed-4509-bc2a-da10c9d83e91' -- Resolution for "bikes and scooters"
            ])),
    labels as (
        SELECT item_id,
               json_agg(label) as labels
        FROM labels_flat
        GROUP BY item_id),
    ulabels_flat as (
        SELECT i.item_id,
               json_build_object(a.key, a.value_int) as label
        FROM item i
             JOIN annotation a ON a.item_id = i.item_id
             JOIN assignment ass ON a.assignment_id = ass.assignment_id
        WHERE i.project_id = '3d761435-fe4f-4829-a33e-26fc52a46613'
          AND a.key = ANY (array [
            'incl','cluster',
            'imp_build', 'imp_trans', 'imp_uf', 'imp_waste', 'imp_meta'
            ])
          AND ass.assignment_scope_id::text = ANY (array [
            '8fc1f9b3-7ed1-4af1-a2d9-8d1744827dac', -- 20240715_prioritised_bikes_JH_NM_FC
            '00afa75f-4e1f-49db-b6d3-8a98ad2138c3', -- 20240715_prioritised_build_JH_NM_FC
            '145c4809-3d18-482a-81f3-a55ace81ace4', -- 20240715_prioritised_trans_JH_NM_FC
            'bbac2f15-ac3d-4ff0-b054-291a7c117a26', -- 20240715_prioritised_urban_JH_NM_FC
            '27ed2842-0e23-4bae-88ae-f8b170f80ecc', -- 20240715_prioritised_waste_JH_NM_FC
            'c812842c-8e1c-45ba-a8aa-7c26e31066b0', -- 20240717_prioritised_bikes_JH_NM_FP
            'f8e62b5d-4b1e-4266-90e8-8475e3fcc70f', -- 20240717_prioritised_build_JH_NM_FP
            'ce2dcbd3-5b98-4505-920c-ae0857c5a186', -- 20240717_prioritised_trans_JH_NM_FP
            'c15d03ce-3b00-4169-bd1a-58cef9772d72', -- 20240717_prioritised_urban_JH_NM_FP
            'b0c734f7-8f27-4d7c-a78c-b7b8f57a3745' -- 20240717_prioritised_waste_JH_NM_FP
            ])),
    ulabels as (
        SELECT item_id,
               json_agg(label) as labels
        FROM ulabels_flat
        GROUP BY item_id),
    imports as (
        SELECT item_id,
               array_agg(DISTINCT import_id) as imports
        FROM m2m_import_item mii
        GROUP BY item_id)
SELECT i.item_id,
       i.text              as abstract,
       ai.title,
       ai.publication_year as py,
       ai.wos_id,
       ai.doi,
       labels.labels,
       ulabels.labels      as user_labels,
       imports.imports
FROM item i
     JOIN academic_item ai on i.item_id = ai.item_id
     LEFT OUTER JOIN labels ON i.item_id = labels.item_id
     LEFT OUTER JOIN ulabels ON i.item_id = ulabels.item_id
     LEFT OUTER JOIN imports ON i.item_id = imports.item_id
WHERE i.project_id = '3d761435-fe4f-4829-a33e-26fc52a46613';

-- scopes
-- array['ec384280-a445-43de-b467-2cce7a048391',     'dbe0545e-68a6-43e5-b08c-8639281b24bb',     '67ec81fe-ebce-438c-8f6e-519b2f4b50ff',      'd4e5c1b0-d2c5-4a77-877b-5226ead1f8bf']
-- labels
-- array['be1361b6-35ed-4509-bc2a-da10c9d83e91',   'c42b71c7-f1dc-46b2-b187-72fa78f8a397',  'c77b8462-7ab5-4bac-b686-6008c8fbe727', '0057dc42-5821-445b-b3d5-834d0958d110',]
WITH
    scopes as (
        SELECT scope_id::uuid,
               row_number() OVER () AS scope_order
        FROM unnest(:scopes ::uuid[]) as scope_id),
    labels_flat as (
        SELECT ba.item_id,
               ba."order",
               scope.scope_order,
               json_object_agg(ba.key,
                               json_build_object('bool', ba.value_bool,
                                                 'int', ba.value_int,
                                                 'multi', ba.multi_int)) as label
        FROM bot_annotation ba
             JOIN scopes scope ON scope.scope_id = ba.bot_annotation_metadata_id
        GROUP BY ba.item_id, ba."order", scope.scope_order),
    labels as (
        SELECT item_id,
               min(scope_order) as scope_order,
               min("order")     as item_order,
               json_agg(label)  as labels
        FROM labels_flat
        GROUP BY item_id),
    ulabels_flat as (
        SELECT ass.item_id,
               ass."order",
               scope.scope_order,
               u.username,
               json_object_agg(a.key,
                               json_build_object('bool', a.value_bool,
                                                 'int', a.value_int,
                                                 'multi', a.multi_int)) as label
        FROM annotation a
             JOIN "user" u ON u.user_id = a.user_id
             JOIN assignment ass ON a.item_id = ass.item_id
             JOIN scopes scope ON scope.scope_id = ass.assignment_scope_id
        GROUP BY ass.item_id, ass."order", scope.scope_order, u.username),
    ulabels as (
        SELECT item_id,
               min(scope_order)                 as scope_order,
               min("order")                     as item_order,
               json_object_agg(username, label) as labels
        FROM ulabels_flat
        GROUP BY item_id),
    imports as (
        SELECT item_id,
               array_agg(DISTINCT import_id) as imports
        FROM m2m_import_item mii
        GROUP BY item_id)
SELECT i.item_id,
       i.text                                            as abstract,
       ai.title,
       ai.publication_year                               as py,
       ai.wos_id,
       ai.doi,
       labels.labels                                     as labels_resolved,
       ulabels.labels                                    as labels_unresolved,
       imports.imports,
       coalesce(labels.scope_order, ulabels.scope_order) as scope_order,
       coalesce(labels.item_order, ulabels.item_order)   as item_order
FROM item i
     JOIN academic_item ai on i.item_id = ai.item_id
     LEFT OUTER JOIN labels ON i.item_id = labels.item_id
     LEFT OUTER JOIN ulabels ON i.item_id = ulabels.item_id
     LEFT OUTER JOIN imports ON i.item_id = imports.item_id
WHERE i.project_id = :project_id
ORDER BY scope_order, item_order
LIMIT 10;



SELECT a.*
FROM annotation a;

DELETE FROM bot_annotation WHERE bot_annotation_metadata_id='9bf07228-9745-4c5e-895c-5450527e5a22';