import asyncio
import logging

import pandas as pd
import numpy as np

from nacsos_data.db import get_engine_async
from nacsos_data.models.items.academic import AcademicItemModel, AcademicAuthorModel
from nacsos_data.util.academic.duplicate import str_to_title_slug
from nacsos_data.util.academic.importer import import_academic_items
from nacsos_data.util.academic.readers.wosfile import read

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('import')
logger.setLevel(logging.DEBUG)

PROJECT_ID = '3d761435-fe4f-4829-a33e-26fc52a46613'
JOSEFINE = '45c63ef0-2af7-4652-b096-cff368497fea'
FILES = [
    ('data/wos/r_uf_22-24.txt', 'Updated Query (11.07.2024) | >= 2022 | Urban form'),
    ('data/wos/r_bikes_22-24_1.txt', 'Updated Query (11.07.2024) | >= 2022 | Bikes'),
    ('data/wos/r_transport_22-24.txt', 'Updated Query (11.07.2024) | >= 2022 | Transport'),
    ('data/wos/r_transport_until 22.txt', 'Updated Query (11.07.2024) | < 2022 | Transport'),
    ('data/wos/r_buildings_until 22.txt', 'Updated Query (11.07.2024) | < 2022 | Buildings'),
    ('data/wos/r_buildings_22-24.txt', 'Updated Query (11.07.2024) | >= 2022 | Buildings'),
    ('data/wos/r_bikes_until 2022_1.txt', 'Updated Query (11.07.2024) | < 2022 | Bikes'),
    ('data/wos/r_waste_until 22.txt', 'Updated Query (11.07.2024) | < 2022 | Waste'),
    ('data/wos/r_waste_22-24.txt', 'Updated Query (11.07.2024) | >= 2022 | Waste'),
    ('data/wos/r_uf_until 22.txt', 'Updated Query (11.07.2024) | < 2022 | Urban form'),
]


def generate_items(path: str):
    def gen():
        yield from read_wos_file(path, PROJECT_ID)

    return gen


async def importer():
    db_engine = get_engine_async(conf_file='../.config/remote.env')

    for path, name in FILES:
        logger.info(f'Importing from file {path} for "{name}"')

        await import_academic_items(
            db_engine=db_engine,
            project_id=PROJECT_ID,
            new_items=generate_items(path),
            import_name=name,
            description=f'Data retrieved on 2024-07-11 from WoS with updated queries',
            user_id=JOSEFINE,
            import_id=None,
            vectoriser=None,
            max_slop=0.05,
            batch_size=5000,
            dry_run=False,
            trust_new_authors=False,
            trust_new_keywords=False,
            log=logger
        )


if __name__ == '__main__':
    asyncio.run(importer())
