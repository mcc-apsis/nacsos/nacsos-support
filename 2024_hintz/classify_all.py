SCHEME = {
    "cluster": [
        {
            "name": "Buildings",
            "value": 0,
        },
        {
            "name": "Transport",
            "value": 1,
        },
        {
            "name": "Urban form",
            "value": 2,
        },
        {
            "name": "Waste",
            "value": 3,
        },
        {
            "name": "Meta-characteristics",
            "value": 4,
        }
    ],
    'imp_build': [
        {
            "name": "Appliance efficiency",
            "hint": "Appliances that can perform a given task needed less energy.",
            "value": 0,
        },
        {
            "name": "Building-level energy consumption",
            "hint": "Focus on overall emissions of building and the reduction thereof, focus on mapping/understanding where high consumption settings are as opposed to actively trying to reduce known high consumption settings.",
            "value": 1,
        },
        {
            "name": "Building retrofit envelope",
            "hint": "Retrofit approach with added insulation and an air barrier around existing walls to improve thermal comfort and reduce drafts.",
            "value": 2,
        },
        {
            "name": "Consumer behavior",
            "hint": "Emission reduction due to changed consumption behavior of residents, linked to appliance efficiency and fuel switch.",
            "value": 3,
        },
        {
            "name": "Demand response",
            "hint": "Change in electricity consumption by end-user(s) (technologies) to help balance the electricity grid or benefit from periods of high availability of renewable energy.",
            "value": 4,
        },
        {
            "name": "District heating and cooling",
            "hint": "Grouped supply for heating and/or cooling for several (typically residential or commercial) buildings, leading to a more energy-efficient supply.",
            "value": 5,
        },
        {
            "name": "Fuel switch",
            "hint": "Changing the type of energy, electricity, and gas, fueling the building  operations e.g. from fossil fuel to renewables.",
            "value": 6,
        },
        {
            "name": "New efficient buildings",
            "hint": "Energy efficient construction and operation as a requirement of newly built stock e.g. via standards.",
            "value": 7,
        },
        {
            "name": "Operational efficiency",
            "hint": "Use of technology that can perform a given task needs less energy e.g. HVAC or lighting.",
            "value": 8,
        }
    ],
    "imp_trans": [
        {
            "name": "Alternative fuels and infrastructure",
            "hint": "Switching to bio-based fuels and establishing respective distribution and charging infrastructure.",
            "value": 9,
        },
        {
            "name": "Autonomous vehicles",
            "hint": "A vehicle capable of sensing its environment and operating without human involvement.",
            "value": 10,
        },
        {
            "name": "Battery-electrified transport",
            "hint": "Enabling or supporting the use of electric vehicles, public and non-public transport.",
            "value": 11,
        },
        {
            "name": "Charging infrastructure",
            "hint": "Putting in place charging points, linked to convenient parking, matching the demand.",
            "value": 12,
        },
        {
            "name": "Consumer behavior",
            "hint": "Impact based on the choices made by users over mode of transport, distance, and number of trips.",
            "value": 13,
        },
        {
            "name": "Freight operations and routing",
            "hint": "Trips for logistics and delivery of goods, can include policies around speed, parking, and allowed hours.",
            "value": 14,
        },
        {
            "name": "Micromobility",
            "hint": "Range of small, lightweight vehicles operating at low speeds.",
            "value": 15,
        },
        {
            "name": "Pedestrian-cyclist interaction",
            "hint": "Includes dynamics between pedestrian flow and cyclists' routes.",
            "value": 16,
        },
        {
            "name": "Not pooled shared mobility",
            "hint": "Vehicle sharing over time, personal rental, no private ownership, shared costs.",
            "value": 17,
        },
        {
            "name": "Pooled shared mobility",
            "hint": "Simultaneous ride-sharing, cost sharing.",
            "value": 18,
        },
        {
            "name": "Public transport O & M",
            "hint": "Changes to operation and maintenance within the existing network.",
            "value": 19,
        },
        {
            "name": "Public transport planning",
            "hint": "Includes conceptualization, ideation, analysis, and design of interventions on existing routes, creation of new routes, and mobility options. Buses, metro, train, trams.",
            "value": 20,
        },
        {
            "name": "Roadway electrification",
            "hint": "Building roads that supply electric power to vehicles e.g. trucks travelling on it.",
            "value": 21,
        },
        {
            "name": "Shared bikes and scooters",
            "hint": "Network of bikes and scooters that can be rented for a period of time by users who pay only for the duration of use.",
            "value": 22,
        },
        {
            "name": "Traffic management and routing",
            "hint": "Optimisation of traffic flow, identification of patterns to improve routing, can include traffic light rhythm.",
            "value": 23,
        },
        {
            "name": "Utility vehicle fleets",
            "hint": "Vehicles for specific tasks such as waste trucks, street cleaning vehicles, maintenance operations, and snow trucks.",
            "value": 24,
        },
        {
            "name": "Vehicle efficiency",
            "hint": "Improvements in driving cycles and fuel consumption.",
            "value": 25,
        }
    ],
    "imp_uf": [
        {
            "name": "Spatial configuration and transport energy consumption",
            "hint": "Dependencies between spatial configurations and transport energy consumption.",
            "value": 26,
        },
        {
            "name": "Urban & neighborhood design",
            "hint": "Includes place-making, master planning, and interventions based on the design process.",
            "value": 27,
        },
        {
            "name": "Utility infrastructure planning",
            "hint": "System-level conceptualization/ configuration of water and energy grids and other resource flows.",
            "value": 28,
        },
        {
            "name": "Zoning, land use, green space",
            "hint": "Specifications around the type of activities allowed or intended e.g. industrial, residential, mixed-use. often based on local plans or planning policies. zoning is common in the USA.",
            "value": 29,
        },
        {
            "name": "Street network design",
            "hint": "Process to determine hierarchies between streets, linkages, and crossings. Both at the neighborhood and city levels.",
            "value": 30,
        },
        {
            "name": "CO2 emission & energy consumption prediction",
            "hint": "Mapping CO2 emissions in cities across sectors and linking their values to urban form characteristics.",
            "value": 31,
        },
        {
            "name": "Multi-city comparison",
            "hint": "Comparative studies involving different urban contexts, in the same or different countries.",
            "value": 32,
        },
        {
            "name": "Street lighting",
            "hint": "Agile management and increased efficiency of public lighting  infrastructure.",
            "value": 33,
        }
    ],

    "imp_waste": [
        {
            "name": "Circular economy",
            "hint": "Processes to convert waste to resource, to divert waste from landfill, include recycling, and composting with clear climate mitigation relevance.",
            "value": 34,
        },
        {
            "name": "Municipal solid waste management",
            "hint": "Management of processing waste types consisting of everyday items that are discarded by the public, including collection and transport of waste to the place of treatment or discharge by municipal services.",
            "value": 35,
        },
        {
            "name": "Waste to energy",
            "hint": "Waste treatment processes that creates energy in the form of electricity, heat or transport fuels, biomass gasification.",
            "value": 36,
        },
        {
            "name": "Wastewater management",
            "hint": "Generation of the polluted form of water ie. sewage (rainfall and human activities), treatment as the process used to remove contaminants from wastewater and convert it into an effluent that can be returned to the water cycle.",
            "value": 37,
        },
        {
            "name": "Food loss and waste",
            "hint": "A decrease at all stages of the food system from production to consumption, in mass and/or quality, of food that was originally intended for human consumption, regardless of the cause.",
            "value": 38,
        },
        {
            "name": "Landfill methane emission prediction",
            "hint": "Forecasting amount of methane gas as a natural byproduct of the decomposition of organic material in landfills.",
            "value": 39,
        }
    ],
    "imp_meta": [
        {
            "name": "Modeling method contribution",
            "hint": "Focus on creating or advancing an ML method in accuracy or calculation pace.",
            "value": 40,
        },
        {
            "name": "Dataset provision & Data imputation",
            "hint": "Using ML for data acquisition and substitution.",
            "value": 41,
        },
        {
            "name": "Multi-stakeholder planning process",
            "hint": "Integration of different stakeholder groups like citizens, private or public sector.",
            "value": 42,
        }
    ]
}

IMPACT_KEYS = ['imp_build', 'imp_trans', 'imp_uf', 'imp_waste', 'imp_meta']
IMPACTS = sorted([
    lab
    for key in IMPACT_KEYS
    for lab in SCHEME[key]
], key=lambda e: e['value'])
CLUSTERS = SCHEME['cluster']

import logging
import uuid
import torch
import evaluate
from tqdm import tqdm

import pandas as pd
import numpy as np
from sqlalchemy import select, func, distinct, or_, and_, union, text

from datasets import Dataset
from transformers import AutoTokenizer, AutoModelForSequenceClassification, Trainer, TrainingArguments, AutoModelForMaskedLM

from nacsos_data.db import get_engine
from nacsos_data.db.schemas.imports import Import, m2m_import_item_table
from nacsos_data.db.schemas.items.academic import AcademicItem, AcademicItemVariant
from nacsos_data.db.schemas.items.base import Item
from nacsos_data.db.schemas import AssignmentScope, Assignment, Annotation, BotAnnotationMetaData, BotAnnotation
from nacsos_data.util.academic.duplicate import str_to_title_slug

db_engine = get_engine(conf_file='/usr/share/nacsos/server.env')

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('import')
logger.setLevel(logging.DEBUG)

pd.options.display.max_columns = None

PROJECT_ID = '3d761435-fe4f-4829-a33e-26fc52a46613'
SCHEME_ID = '4658a522-0626-4074-b207-ed46ea13d2a1'

JOSEFINE = '45c63ef0-2af7-4652-b096-cff368497fea'
FELIX = '6f57074c-b145-4826-a20c-2aba8939758f'
LYNN = '8049cefa-6f2a-4fed-8b71-ca9424dd99c6'
NIKOLA = '3cd7dc2a-f920-4699-898e-7760ef7fb063'


df = pd.read_feather('data/full_data.arrow')
df['incl'] = df['incl'].astype('Int8')
df['cluster'] = df['cluster'].astype('Int8')
df['impact'] = df['impact'].astype('Int8')
df['py'] = df['py'].astype('Int16')
df = df.replace({np.nan: None})

SETTINGS = []

def compute_metrics1(p):
    logits, labels = p
    predictions = np.argmax(logits, axis=-1)
    return {
        'recall': evaluate.load('recall').compute(predictions=predictions, references=labels, zero_division=0, average='weighted')['recall'],
        'precision': evaluate.load('precision').compute(predictions=predictions, references=labels, zero_division=0, average='weighted')['precision'],
        'f1': evaluate.load('f1').compute(predictions=predictions, references=labels,  labels=np.arange(len(labels)), average='weighted')['f1'],#zero_division=0,
        'accuracy': evaluate.load('accuracy').compute(predictions=predictions, references=labels, normalize=False)['accuracy']
    }

def compute_metrics2(p):
    logits, labels = p
    predictions = np.argmax(logits, axis=-1)
    return {
        'recall': evaluate.load('recall').compute(predictions=predictions, references=labels, zero_division=0, average='binary')['recall'],
        'precision': evaluate.load('precision').compute(predictions=predictions, references=labels, zero_division=0, average='binary')['precision'],
        'f1': evaluate.load('f1').compute(predictions=predictions, references=labels, labels=np.arange(len(labels)), average='binary')['f1'],
        'accuracy': evaluate.load('accuracy').compute(predictions=predictions, references=labels, normalize=False)['accuracy']
    }
    
# Inclusion
dfi = df[~df['incl'].isna()][['text', 'incl']].copy()
dfi['label'] = dfi['incl']
SETTINGS.append({
    'name': 'Inclusion',
    'key': 'incl',
    'df': dfi,
    'labels': ['Exclude', 'Include'],
    'cols': ['pred_incl|0', 'pred_incl|1'],
    'agg_col': 'pred_incl',
    'frac': 0.8,
    'cm': compute_metrics2,
})

# Cluster
dfc = df[~df['cluster'].isna()][['text', 'cluster']].copy()
dfc['label'] = dfc['cluster']
SETTINGS.append({
    'name': 'Impact cluster',
    'key': 'cluster',
    'df': dfc,
    'labels': ['Buildings', 'Transport', 'Urban form', 'Waste', 'Meta-characteristics'],
    'cols': ['pred_cluster|0', 'pred_cluster|1', 'pred_cluster|2', 'pred_cluster|3', 'pred_cluster|4'],
    'agg_col': 'pred_cluster',
    'frac': 0.8,
    'cm': compute_metrics1,
})

# Buildings
dfbcols = [f'impact|{c['value']}' for c in SCHEME['imp_build']]
dfb = df[df['cluster'] == 0][dfbcols + ['text']].copy()
dfb['label'] = dfb[dfbcols].replace({None: 0}).to_numpy().argmax(axis=1)
SETTINGS.append({
    'name': 'Buildings',
    'key': 'buildings',
    'df': dfb,
    'labels': ['Appliance efficiency', 'Building-level energy consumption', 'Building retrofit envelope', 
               'Consumer behavior', 'Demand response', 'District heating and cooling', 'Fuel switch', 
               'New efficient buildings', 'Operational efficiency'], # [c['name'] for c in SCHEME['imp_build']],
    'cols': ['pred_impact|0', 'pred_impact|1', 'pred_impact|2', 'pred_impact|3', 'pred_impact|4',
             'pred_impact|5', 'pred_impact|6', 'pred_impact|7', 'pred_impact|8'],
    'agg_col': 'pred_build',
    'frac': 0.9,
    'cm': compute_metrics1,
})

# Transport
dftcols = ['impact|9', 'impact|10', 'impact|11', 'impact|12', 'impact|13', 'impact|14', 'impact|15', 
           'impact|17', 'impact|18', 'impact|19', 'impact|20',  'impact|22', 'impact|23', 'impact|24', 'impact|25']  # 'impact|16', 'impact|21',
dft = df[df['cluster'] == 1][dftcols + ['text']].copy()
dft['label'] = dft[dftcols].replace({None: 0}).to_numpy().argmax(axis=1)
SETTINGS.append({
    'name': 'Transport',
    'key': 'trans',
    'df': dft,
    'labels': ['Alternative fuels and infrastructure', 'Autonomous vehicles', 'Battery-electrified transport',
               'Charging infrastructure', 'Consumer behavior', 'Freight operations and routing', 'Micromobility', 
                'Not pooled shared mobility', 'Pooled shared mobility', # 'Pedestrian-cyclist interaction',
               'Public transport O & M', 'Public transport planning', #'Roadway electrification',
               'Shared bikes and scooters', 'Traffic management and routing', 'Utility vehicle fleets', 'Vehicle efficiency'],
    'cols': ['pred_impact|9', 'pred_impact|10', 'pred_impact|11', 'pred_impact|12', 'pred_impact|13', 'pred_impact|14', 
             'pred_impact|15', 'pred_impact|17', 'pred_impact|18', 'pred_impact|19', 'pred_impact|20', 'pred_impact|22',
             'pred_impact|23', 'pred_impact|24', 'pred_impact|25'],
    'agg_col': 'pred_impact',
    'frac': 0.9,
    'cm': compute_metrics1,
})

# Urban form
dffcols = ['impact|27', 'impact|28', 'impact|29',  'impact|31', 'impact|32']  # 'impact|26', 'impact|30', 'impact|33',
dff = df[df['cluster'] == 2][dffcols + ['text']].copy()
dff['label'] = dff[dffcols].replace({None: 0}).to_numpy().argmax(axis=1)
SETTINGS.append({
    'name': 'Urban form',
    'key': 'uf',
    'df': dff,
    'labels': ['Urban & neighborhood design',  # 'Spatial configuration and transport energy consumption',
               'Utility infrastructure planning', 'Zoning, land use, green space', #'Street network design', 
               'CO2 emission & energy consumption prediction', 'Multi-city comparison'],  # , 'Street lighting'
    'cols': ['pred_impact|27', 'pred_impact|28', 'pred_impact|29',# 'pred_impact|30', 
             'pred_impact|31', 'pred_impact|32'],  #, 'pred_impact|33'
    'agg_col': 'pred_uf',
    'frac': 0.9,
    'cm': compute_metrics1,
})

# Waste
dfwcols = ['impact|34', 'impact|35', 'impact|36', 'impact|37', 'impact|38']  # 'impact|39'
dfw = df[df['cluster'] == 3][dfwcols + ['text']].copy()
dfw['label'] = dfw[dfwcols].replace({None: 0}).to_numpy().argmax(axis=1)
SETTINGS.append({
    'name': 'Waste',
    'key': 'waste',
    'df': dfw,
    'labels': ['Circular economy', 'Municipal solid waste management', 'Waste to energy',
               'Wastewater management', 'Food loss and waste'], # , 'Landfill methane emission prediction'
    'cols': ['pred_impact|34', 'pred_impact|35', 'pred_impact|36', 'pred_impact|37', 'pred_impact|38'],
    'agg_col': 'pred_waste',
    'frac': 0.9,
    'cm': compute_metrics1,
})

# Meta
dfmcols = ['impact|40', 'impact|41', 'impact|42']
dfm = df[df['cluster'] == 4][dfmcols + ['text']].copy()
dfm['label'] = dfm[dfmcols].replace({None: 0}).to_numpy().argmax(axis=1)
SETTINGS.append({
    'name': 'Meta',
    'key': 'meta',
    'df': dfm,
    'labels': ['Modeling method contribution', 'Dataset provision & Data imputation', 'Multi-stakeholder planning process'],
    'cols': ['pred_impact|40', 'pred_impact|41', 'pred_impact|42'],
    'agg_col': 'pred_meta',
    'frac': 0.9,
    'cm': compute_metrics1,
})

# print([f'impact|{c['value']}' for c in SCHEME['imp_meta']])
# dfmcols = ['impact|40', 'impact|41', 'impact|42']
# dfm = df[df['cluster'] == 3][dfmcols + ['text']].copy()
# print([f'pred_{c}'for c in dfmcols])
# print([c['name'] for c in SCHEME['imp_meta']])
# for c in SCHEME['imp_waste']:
#    print(c['value'], c['name'])


for setting in SETTINGS:
    print('=========================================================')
    print(f'Running for {setting['name']} with {setting['df'].shape}')
    print('=========================================================')
    
    df_train = setting['df'].sample(frac=setting['frac'])

    mask_train = setting['df'].index.isin(df_train.index)
    mask_test = ~mask_train

    df_test = setting['df'][mask_test]
    labels = list(setting['df']['label'].unique())
    
    print('values', labels)
    print('labels', setting['labels'])
    print('columns', setting['cols'])
    print('mask train', mask_train.sum(), 'test', mask_test.sum())
    print('df train', df_train.shape, 'test', df_test.shape)
    print()
    print('train')
    print(df_train['label'].value_counts())
    print('test')
    print(df_test['label'].value_counts())


model_name = 'climatebert/distilroberta-base-climate-f'
tokenizer = AutoTokenizer.from_pretrained(model_name, max_length=512, model_max_length=512)

BATCH_SIZE_PRED = 50

for setting in SETTINGS:
    print('=========================================================')
    print(f'Running for {setting['name']} with {setting['df'].shape}')
    print('=========================================================')
    
    df_train = setting['df'].sample(frac=setting['frac'])

    mask_train = setting['df'].index.isin(df_train.index)
    mask_test = ~mask_train

    df_test = setting['df'][mask_test]
    labels = list(setting['df']['label'].unique())
    
    print('values', labels)
    print('labels', setting['labels'])
    print('columns', setting['cols'])
    print('mask train', mask_train.sum(), 'test', mask_test.sum())
    print('df train', df_train.shape, 'test', df_test.shape)
    print()
    print('train')
    print(df_train['label'].value_counts())
    print('test')
    print(df_test['label'].value_counts())

    train_dataset = Dataset.from_pandas(df_train)
    train_dataset = train_dataset.map(lambda rows: tokenizer(rows['text'], padding='max_length', truncation=True), batched=True)
    
    eval_dataset = Dataset.from_pandas(df_test)
    eval_dataset = eval_dataset.map(lambda rows: tokenizer(rows['text'], padding='max_length', truncation=True), batched=True)
    
    print(len(train_dataset), len(eval_dataset))

    print('Loading model...')
    model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=len(labels))

    # Define training arguments
    training_args = TrainingArguments(
        output_dir='data/results',
        logging_dir='data/logs',
        num_train_epochs=4,
        per_device_train_batch_size=16,
        per_device_eval_batch_size=50,
        warmup_steps=400,
        weight_decay=0.01,
        logging_steps=10,
        eval_strategy='steps',
        eval_steps=50,
    )

    # Initialize Trainer
    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=train_dataset,
        eval_dataset=eval_dataset,
        compute_metrics=setting['cm'],
    )
    
    # Train the model
    print('Training model...')
    trainer.train()

    print('Predicting...')
    preds = []
    with torch.no_grad():
        ds = Dataset.from_pandas(df)
        ds = ds.map(lambda x: tokenizer(x['text'], padding='max_length', truncation=True), batched=True)
        ds.set_format('torch')
    
        for batch in tqdm(ds.iter(batch_size=BATCH_SIZE_PRED)):
            pred = model(input_ids=batch['input_ids'].to('cuda'), attention_mask=batch['attention_mask'].to('cuda'))
            preds.append(torch.softmax(pred.logits, dim=1).cpu())
    
    preds = torch.concatenate(preds)
    df[setting['agg_col']] = preds.argmax(dim=1)
    for li, c in enumerate(setting['cols']):
        df[c] = preds[:,li]

    df.to_feather(f'data/data_pred_{setting['key']}.arrow')