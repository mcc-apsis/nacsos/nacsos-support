import asyncio
import logging

import pandas as pd
import numpy as np

from nacsos_data.db import get_engine_async
from nacsos_data.models.items.academic import AcademicItemModel, AcademicAuthorModel
from nacsos_data.util.academic.duplicate import str_to_title_slug
from nacsos_data.util.academic.importer import import_academic_items

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('import')
logger.setLevel(logging.DEBUG)

dfs = pd.read_excel('data/AllRecords.xlsx', sheet_name=None)
PROJECT_ID = '3d761435-fe4f-4829-a33e-26fc52a46613'
JOSEFINE = '45c63ef0-2af7-4652-b096-cff368497fea'


def generate_items(key: str):
    def gen():
        df = dfs[key]
        df = df.replace({np.nan: None})

        for ri, row in df.iterrows():
            keywords_str = (row['Keywords Author'] or '') + ';' + (row['Keywords Plus'] or '') + ';' + (
                    row['WoS Categories'] or '')
            keywords = [kw.strip() for kw in keywords_str.split(';')]
            keywords = [kw for kw in keywords if len(kw) > 0]
            title = row['Title']
            authors = row['Authors'].split(';')
            authors = [AcademicAuthorModel(name=a.strip()) for a in authors]

            yield AcademicItemModel(
                title=title,
                title_slug=str_to_title_slug(title),
                text=row['Abstract'],
                publication_year=row['Year'],
                doi=row['DOI'],
                authors=authors,
                keywords=keywords if len(keywords) > 0 else None,
                source=row['Journal'],
                meta={
                    'document_type': row['Document Type'],
                    'publication_type': row['Publication Type'],
                    'author_address': row['Author Address'],
                    'keywords_author': row['Keywords Author'],
                    'keywords_plus': row['Keywords Plus'],
                    'wos_categories': row['WoS Categories'],
                    'source': f'{key}_{ri}'
                }
            )

    return gen


def test_parsing():
    for k in dfs.keys():
        generator = generate_items(k)
        items = list(generator())
        logger.info(len(items))


async def importer():
    db_engine = get_engine_async(conf_file='../.config/remote.env')

    for k in dfs.keys():
        logger.info('Importing from sheet {k}')
        async with db_engine.session() as session:
            await import_academic_items(
                session=session,
                project_id=PROJECT_ID,
                new_items=generate_items(k),
                import_name=f'Original data ({k})',
                description=f'Data from the AllData.xlsx sheet {k}',
                user_id=JOSEFINE,
                import_id=None,
                vectoriser=None,
                max_slop=0.05,
                batch_size=5000,
                dry_run=False,
                trust_new_authors=False,
                trust_new_keywords=False,
                log=logger
            )


def list_columns():
    pc = set()
    for k, df in dfs.items():
        # sheet name
        print(k)

        # current columns
        cc = set(df.columns)
        print(df.columns)

        # difference to prior sheet
        print(cc - pc)
        print(pc - cc)
        pc = cc


def list_values(col: str = 'Relevance'):
    acc = None
    for k, df in dfs.items():
        print(k)
        cnts = df[col].value_counts()
        print(cnts)
        if acc is None:
            acc = cnts
        else:
            acc.add(cnts, fill_value=0)
    print('Total')
    print(acc)


if __name__ == '__main__':
    asyncio.run(importer())
