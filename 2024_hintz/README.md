**AI for adaptation in cities**   
**Project ID:** 3d761435-fe4f-4829-a33e-26fc52a46613    
*Marie Josefine Hintz, 2024*    
Revision of an article, annotating more data...    

Contents:
* `import_xls.py`: Import originally coded data from XLS file
* `Stats.ipynb`: Some statistics after importing recent data from WoS (counts, histogram, import/query overlap)