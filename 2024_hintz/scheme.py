scheme = {
    "name": "Cluster",
    "key": "cluster",
    "hint": "",
    "max_repeat": 3,
    "required": False,
    "dropdown": False,
    "kind": "single",
    "choices": [
        {
            "name": "Buildings",
            "hint": None,
            "value": 0,
            "children": [
                {
                    "name": "Impact area",
                    "key": "imp_build",
                    "hint": "",
                    "max_repeat": 1,
                    "required": False,
                    "dropdown": False,
                    "kind": "single",
                    "choices": [
                        {
                            "name": "Appliance efficiency",
                            "orig": ["appliance efficiency"],
                            "hint": "Appliances that can perform a given task needed less energy.",
                            "value": 0,
                            "children": None
                        },
                        {
                            "name": "Building-level energy consumption",
                            "orig": ["building-level", "Building-level"],
                            "hint": "Focus on overall emissions of building and the reduction thereof, focus on mapping/understanding where high consumption settings are as opposed to actively trying to reduce known high consumption settings.",
                            "value": 1,
                            "children": None
                        },
                        {
                            "name": "Building retrofit envelope",
                            "orig": ["retrofit envelope", "Retrofit envelope", "retrofit envelope\nnew buildings"],
                            "hint": "Retrofit approach with added insulation and an air barrier around existing walls to improve thermal comfort and reduce drafts.",
                            "value": 2,
                            "children": None
                        },
                        {
                            "name": "Consumer behavior",
                            "orig": ["consumer behavior", "Consumer behavior", "consumer behavior(buildings)", "Consumer behavior(buildings)", "consumer behavior (buildings)"],
                            "hint": "Emission reduction due to changed consumption behavior of residents, linked to appliance efficiency and fuel switch.",
                            "value": 3,
                            "children": None
                        },
                        {
                            "name": "Demand response",
                            "orig": ["demand response", "Demand response"],
                            "hint": "Change in electricity consumption by end-user(s) (technologies) to help balance the electricity grid or benefit from periods of high availability of renewable energy.",
                            "value": 4,
                            "children": None
                        },
                        {
                            "name": "District heating and cooling",
                            "orig": ["district heating and cooling", "District heating and cooling"],
                            "hint": "Grouped supply for heating and/or cooling for several (typically residential or commercial) buildings, leading to a more energy-efficient supply.",
                            "value": 5,
                            "children": None
                        },
                        {
                            "name": "Fuel switch",
                            "orig": ["fuel switch", "Fuel switch"],
                            "hint": "Changing the type of energy, electricity, and gas, fueling the building  operations e.g. from fossil fuel to renewables.",
                            "value": 6,
                            "children": None
                        },
                        {
                            "name": "New efficient buildings",
                            "orig": ["new buildings"],
                            "hint": "Energy efficient construction and operation as a requirement of newly built stock e.g. via standards.",
                            "value": 7,
                            "children": None
                        },
                        {
                            "name": "Operational efficiency",
                            "orig": ["operational efficiency", "Operational efficiency"],
                            "hint": "Use of technology that can perform a given task needs less energy e.g. HVAC or lighting.",
                            "value": 8,
                            "children": None
                        }
                    ],
                    "annotation": None
                }
            ]
        },
        {
            "name": "Transport",
            "hint": None,
            "value": 1,
            "children": [
                {
                    "name": "Impact area",
                    "key": "imp_trans",
                    "hint": "",
                    "max_repeat": 1,
                    "required": False,
                    "dropdown": True,
                    "kind": "single",
                    "choices": [
                        {
                            "name": "Alternative fuels and infrastructure",
                            "orig": ["alternative fuels and infrastructure"],
                            "hint": "Switching to bio-based fuels and establishing respective distribution and charging infrastructure.",
                            "value": 9,
                            "children": None
                        },
                        {
                            "name": "Autonomous vehicles",
                            "orig": ["autonomous vehicles"],
                            "hint": "A vehicle capable of sensing its environment and operating without human involvement.",
                            "value": 10,
                            "children": None
                        },
                        {
                            "name": "Battery-electrified transport",
                            "orig": ["battery-electrified transport"],
                            "hint": "Enabling or supporting the use of electric vehicles, public and non-public transport.",
                            "value": 11,
                            "children": None
                        },
                        {
                            "name": "Charging infrastructure",
                            "orig": ["charging infrastructure", "Charging infrastructure"],
                            "hint": "Putting in place charging points, linked to convenient parking, matching the demand.",
                            "value": 12,
                            "children": None
                        },
                        {
                            "name": "Consumer behavior",
                            "orig": ["consumer behavior(transportation)"],
                            "hint": "Impact based on the choices made by users over mode of transport, distance, and number of trips.",
                            "value": 13,
                            "children": None
                        },
                        {
                            "name": "Freight operations and routing",
                            "orig": ["freight operations and routing"],
                            "hint": "Trips for logistics and delivery of goods, can include policies around speed, parking, and allowed hours.",
                            "value": 14,
                            "children": None
                        },
                        {
                            "name": "Micromobility",
                            "orig": ["micromobility", "Micromobility"],
                            "hint": "Range of small, lightweight vehicles operating at low speeds.",
                            "value": 15,
                            "children": None
                        },
                        {
                            "name": "Pedestrian-cyclist interaction",
                            "orig": ["pedestrian-cyclists interaction"],
                            "hint": "Includes dynamics between pedestrian flow and cyclists' routes.",
                            "value": 16,
                            "children": None
                        },
                        {
                            "name": "Not pooled shared mobility",
                            "orig": ["not pool. shared mobility (cars)", "not pool. shared mobility'"],
                            "hint": "Vehicle sharing over time, personal rental, no private ownership, shared costs.",
                            "value": 17,
                            "children": None
                        },
                        {
                            "name": "Pooled shared mobility",
                            "orig": ["pooled shared mobility"],
                            "hint": "Simultaneous ride-sharing, cost sharing.",
                            "value": 18,
                            "children": None
                        },
                        {
                            "name": "Public transport O & M",
                            "orig": ["public transport O&M"],
                            "hint": "Changes to operation and maintenance within the existing network.",
                            "value": 19,
                            "children": None
                        },
                        {
                            "name": "Public transport planning",
                            "orig": ["public transport planning"],
                            "hint": "Includes conceptualization, ideation, analysis, and design of interventions on existing routes, creation of new routes, and mobility options. Buses, metro, train, trams.",
                            "value": 20,
                            "children": None
                        },
                        {
                            "name": "Roadway electrification",
                            "orig": [],
                            "hint": "Building roads that supply electric power to vehicles e.g. trucks travelling on it.",
                            "value": 21,
                            "children": None
                        },
                        {
                            "name": "Shared bikes and scooters",
                            "orig": ["shared bikes/scooters etc."],
                            "hint": "Network of bikes and scooters that can be rented for a period of time by users who pay only for the duration of use.",
                            "value": 22,
                            "children": None
                        },
                        {
                            "name": "Traffic management and routing",
                            "orig": ["traffic management/routing/congestion"],
                            "hint": "Optimisation of traffic flow, identification of patterns to improve routing, can include traffic light rhythm.",
                            "value": 23,
                            "children": None
                        },
                        {
                            "name": "Utility vehicle fleets",
                            "orig": ["utility vehicle fleets"],
                            "hint": "Vehicles for specific tasks such as waste trucks, street cleaning vehicles, maintenance operations, and snow trucks.",
                            "value": 24,
                            "children": None
                        },
                        {
                            "name": "Vehicle efficiency",
                            "orig": ["vehicle efficiency", "vehicule efficiency"],
                            "hint": "Improvements in driving cycles and fuel consumption.",
                            "value": 25,
                            "children": None
                        }
                    ],
                    "annotation": None
                }
            ]
        },
        {
            "name": "Urban form",
            "hint": None,
            "value": 2,
            "children": [
                {
                    "name": "Impact area",
                    "key": "imp_uf",
                    "hint": "",
                    "max_repeat": 1,
                    "required": False,
                    "dropdown": False,
                    "kind": "single",
                    "choices": [
                        {
                            "name": "Spatial configuration and transport energy consumption",
                            "orig": ["Spatial configurations/travel patterns"],
                            "hint": "Dependencies between spatial configurations and transport energy consumption.",
                            "value": 26,
                            "children": None
                        },
                        {
                            "name": "Urban & neighborhood design",
                            "orig": ["Urban/neighborhood design"],
                            "hint": "Includes place-making, master planning, and interventions based on the design process.",
                            "value": 27,
                            "children": None
                        },
                        {
                            "name": "Utility infrastructure planning",
                            "orig": ["Utility infrastructure planning"],
                            "hint": "System-level conceptualization/ configuration of water and energy grids and other resource flows.",
                            "value": 28,
                            "children": None
                        },
                        {
                            "name": "Zoning, land use, green space",
                            "orig": ["Zoning/land use/green space"],
                            "hint": "Specifications around the type of activities allowed or intended e.g. industrial, residential, mixed-use. often based on local plans or planning policies. zoning is common in the USA.",
                            "value": 29,
                            "children": None
                        },
                        {
                            "name": "Street network design",
                            "orig": ["Street network design"],
                            "hint": "Process to determine hierarchies between streets, linkages, and crossings. Both at the neighborhood and city levels.",
                            "value": 30,
                            "children": None
                        },
                        {
                            "name": "CO2 emission & energy consumption prediction",
                            "orig": ["CO2 emission/energy consumption prediction"],
                            "hint": "Mapping CO2 emissions in cities across sectors and linking their values to urban form characteristics.",
                            "value": 31,
                            "children": None
                        },
                        {
                            "name": "Multi-city comparison",
                            "orig": ["Multi-city study"],
                            "hint": "Comparative studies involving different urban contexts, in the same or different countries.",
                            "value": 32,
                            "children": None
                        },
                        {
                            "name": "Street lighting",
                            "orig": ["Street lighting", "Street lighting", "smart lighting system", "street lighting"],
                            "hint": "Agile management and increased efficiency of public lighting infrastructure.",
                            "value": 33,
                            "children": None
                        }
                    ],
                    "annotation": None
                }
            ]
        },
        {
            "name": "Waste",
            "hint": None,
            "value": 3,
            "children": [
                {
                    "name": "Impact area",
                    "key": "imp_waste",
                    "hint": "",
                    "max_repeat": 1,
                    "required": False,
                    "dropdown": False,
                    "kind": "single",
                    "choices": [
                        {
                            "name": "Circular economy",
                            "orig": ["circular economy"],
                            "hint": "Processes to convert waste to resource, to divert waste from landfill, include recycling, and composting with clear climate mitigation relevance.",
                            "value": 34,
                            "children": None
                        },
                        {
                            "name": "Municipal solid waste management",
                            "orig": ["MSW management", "municipal solid waste management "],
                            "hint": "Management of processing waste types consisting of everyday items that are discarded by the public, including collection and transport of waste to the place of treatment or discharge by municipal services.",
                            "value": 35,
                            "children": None
                        },
                        {
                            "name": "Waste to energy",
                            "orig": ["waste to energy"],
                            "hint": "Waste treatment processes that creates energy in the form of electricity, heat or transport fuels, biomass gasification.",
                            "value": 36,
                            "children": None
                        },
                        {
                            "name": "Wastewater management",
                            "orig": ["wastewater management"],
                            "hint": "Generation of the polluted form of water ie. sewage (rainfall and human activities), treatment as the process used to remove contaminants from wastewater and convert it into an effluent that can be returned to the water cycle.",
                            "value": 37,
                            "children": None
                        },
                        {
                            "name": "Food loss and waste",
                            "orig": ["food loss and waste"],
                            "hint": "A decrease at all stages of the food system from production to consumption, in mass and/or quality, of food that was originally intended for human consumption, regardless of the cause.",
                            "value": 38,
                            "children": None
                        },
                        {
                            "name": "Landfill methane emission prediction",
                            "orig": ["landfill management"],
                            "hint": "Forecasting amount of methane gas as a natural byproduct of the decomposition of organic material in landfills.",
                            "value": 39,
                            "children": None
                        }
                    ],
                    "annotation": None
                }
            ]
        },
        {
            "name": "Meta-characteristics",
            "hint": None,
            "value": 4,
            "children": [
                {
                    "name": "Impact area",
                    "key": "imp_meta",
                    "hint": "",
                    "max_repeat": 1,
                    "required": False,
                    "dropdown": False,
                    "kind": "single",
                    "choices": [
                        {
                            "name": "Modeling method contribution",
                            "orig": ["Modeling method contribution"],
                            "hint": "Focus on creating or advancing an ML method in accuracy or calculation pace.",
                            "value": 40,
                            "children": None
                        },
                        {
                            "name": "Dataset provision & Data imputation",
                            "orig": ["Data provision/imputation"],
                            "hint": "Using ML for data acquisition and substitution.",
                            "value": 41,
                            "children": None
                        },
                        {
                            "name": "Multi-stakeholder planning process",
                            "orig": ["Multi-stakeholder planning process"],
                            "hint": "Integration of different stakeholder groups like citizens, private or public sector.",
                            "value": 42,
                            "children": None
                        }
                    ],
                    "annotation": None
                }
            ]
        }
    ],
    "annotation": None
}

# Unmatched:
# "Siting (others than buildings)"
# "congestion charging"
# "vehicles emissions"

# Unmatched (AllRecords)
# "parking"
# "car-following"
# "combined heat and power"
# "ecodriving"
# "environmental impacts"
# "waste collection"
# "prediction supply"
# "Urban heat island"
# "PV"
# "cost"
# "water prediction block level"
# "multi-modal travel alternatives"
# "parking pricing"
# "waste"
# "tariffs"
# "air pollution"
# "non-motorized"
