# get scheme via https://apsis.mcc-berlin.net/nacsos-core/api/annotations/schemes/definition/a0882206-c5d0-435d-9ee0-51f5bf498680?flat=true
scheme = [
    {
        "name": "Which methods are discussed?",
        "hint": "",
        "key": "meth",
        "required": True,
        "max_repeat": 11,
        "implicit_max_repeat": 11,
        "kind": "multi",
        "choices": [
            {
                "name": "GGR (general)",
                "hint": None,
                "value": 0
            },
            {
                "name": "Direct Air Capture",
                "hint": None,
                "value": 1
            },
            {
                "name": "BECCS",
                "hint": None,
                "value": 2
            },
            {
                "name": "Biochar",
                "hint": None,
                "value": 3
            },
            {
                "name": "Ocean Fertilization",
                "hint": None,
                "value": 4
            },
            {
                "name": "Ocean Alkalinization",
                "hint": None,
                "value": 5
            },
            {
                "name": "Enhanced Weathering",
                "hint": None,
                "value": 6
            },
            {
                "name": "Afforestation/Reforestation",
                "hint": None,
                "value": 7
            },
            {
                "name": "Ecosystem Restoration",
                "hint": None,
                "value": 8
            },
            {
                "name": "Soil Carbon Sequestration",
                "hint": None,
                "value": 9
            },
            {
                "name": "Blue Carbon",
                "hint": None,
                "value": 10
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Does the article mention the need to reduce emissions?",
        "hint": "",
        "key": "red_need",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 0
            },
            {
                "name": "No",
                "hint": None,
                "value": 1
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Does the article mention some of the key sources of greenhouse gas emissions (e.g. fossil fuels, land use)?",
        "hint": "",
        "key": "red_sources",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": "",
                "value": 1
            },
            {
                "name": "No",
                "hint": None,
                "value": 2
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Does the article highlight the importance of emissions reductions, relative to CDR?",
        "hint": "",
        "key": "red_importance",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 1
            },
            {
                "name": "No",
                "hint": None,
                "value": 2
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Does the article explicitly frame CDR as something that should be done instead of reducing emissions?",
        "hint": "",
        "key": "red_delay",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 1
            },
            {
                "name": "No",
                "hint": None,
                "value": 2
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "For what reasons?",
        "hint": "",
        "key": "red_cdr_for",
        "required": False,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "str",
        "choices": [],
        "parent_label": "red_cdr",
        "parent_choice": 0
    },
    {
        "name": "For what reasons?",
        "hint": "",
        "key": "red_cdr_neutral",
        "required": False,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "str",
        "choices": [],
        "parent_label": "red_cdr",
        "parent_choice": 1
    },
    {
        "name": "For what reasons?",
        "hint": "",
        "key": "red_cdr_against",
        "required": False,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "str",
        "choices": [],
        "parent_label": "red_cdr",
        "parent_choice": 2
    },
    {
        "name": "Do you interpret this article as for / neutral / against CDR?",
        "hint": "",
        "key": "red_cdr",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "For",
                "hint": None,
                "value": 0
            },
            {
                "name": "Neutral",
                "hint": None,
                "value": 1
            },
            {
                "name": "Against",
                "hint": None,
                "value": 2
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "And does it distinguish these from CDR?",
        "hint": "",
        "key": "term_offsets_distinguish",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 0
            },
            {
                "name": "No",
                "hint": None,
                "value": 1
            }
        ],
        "parent_label": "term_offsets",
        "parent_choice": 1
    },
    {
        "name": "Does the article appear to discuss avoided emissions or offsets?",
        "hint": "",
        "key": "term_offsets",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 1
            },
            {
                "name": "No",
                "hint": None,
                "value": 2
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "And does it distinguish these from CDR?",
        "hint": "",
        "key": "term_reductions_distinguish",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 0
            },
            {
                "name": "No",
                "hint": None,
                "value": 1
            }
        ],
        "parent_label": "term_reductions",
        "parent_choice": 1
    },
    {
        "name": "Does the article appear to discuss carbon capture processes (e.g. fossil + CCS)?",
        "hint": "",
        "key": "term_reductions",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 1
            },
            {
                "name": "No",
                "hint": None,
                "value": 2
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "And does it distinguish these from CDR?",
        "hint": "",
        "key": "term_utilisation_distinguish",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 0
            },
            {
                "name": "No",
                "hint": None,
                "value": 1
            }
        ],
        "parent_label": "term_utilisation",
        "parent_choice": 1
    },
    {
        "name": "Does the article appear to discuss carbon capture and utilisation processes?",
        "hint": "",
        "key": "term_utilisation",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 1
            },
            {
                "name": "No",
                "hint": None,
                "value": 2
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "And does it distinguish these from CDR?",
        "hint": "",
        "key": "term_srm_distinguish",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 0
            },
            {
                "name": "No",
                "hint": None,
                "value": 1
            }
        ],
        "parent_label": "term_srm",
        "parent_choice": 1
    },
    {
        "name": "Does the article discuss solar radiation management (SRM) or “geoengineering”?",
        "hint": "",
        "key": "term_srm",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 1
            },
            {
                "name": "No",
                "hint": None,
                "value": 2
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Which CDR method is the \"single method\"?",
        "hint": "",
        "key": "context_single_method",
        "required": False,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "str",
        "choices": [],
        "parent_label": "context_single",
        "parent_choice": 0
    },
    {
        "name": "Does the article give the impression that climate change or national emissions can be solved with a \"single CDR method\"?",
        "hint": "",
        "key": "context_single",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 0
            },
            {
                "name": "No",
                "hint": None,
                "value": 1
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Which risks are discussed?",
        "hint": "",
        "key": "risks_what",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "str",
        "choices": [],
        "parent_label": "risks",
        "parent_choice": 0
    },
    {
        "name": "Does the article mention any risks associated with CDR?",
        "hint": "",
        "key": "risks",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 0
            },
            {
                "name": "No",
                "hint": None,
                "value": 1
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Which benefits are discussed?",
        "hint": "",
        "key": "benefits_what",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "str",
        "choices": [],
        "parent_label": "benefits",
        "parent_choice": 0
    },
    {
        "name": "Does the article mention any benefits associated with CDR?",
        "hint": "",
        "key": "benefits",
        "required": True,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "single",
        "choices": [
            {
                "name": "Yes",
                "hint": None,
                "value": 0
            },
            {
                "name": "No",
                "hint": None,
                "value": 1
            }
        ],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Is this an irrelevant article type?",
        "hint": "",
        "key": "irrelevant",
        "required": False,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "bool",
        "choices": [],
        "parent_label": None,
        "parent_choice": None
    },
    {
        "name": "Is this article a duplicate of another you have coded by this scheme?",
        "hint": "",
        "key": "duplicate",
        "required": False,
        "max_repeat": 1,
        "implicit_max_repeat": 1,
        "kind": "bool",
        "choices": [],
        "parent_label": None,
        "parent_choice": None
    }
]

str_labels = [
    label['key']
    for label in scheme
    if label['kind'] == 'str'
]
print(f"STR_LABELS = ['{"', '".join(str_labels)}']")

bool_labels = [
    label['key']
    for label in scheme
    if label['kind'] == 'bool'
]
print(f"BOOL_LABELS = ['{"', '".join(bool_labels)}']")

print('SINGLE_LABELS = {')
for label in scheme:
    if label['kind'] == 'single':
        print(f"  '{label['key']}': [")
        for c in label['choices']:
            print(f"    {c},")
        print('  ],')
print('}')

print('MULTI_LABELS = {')
for label in scheme:
    if label['kind'] == 'multi':
        print(f"  '{label['key']}': [")
        for c in label['choices']:
            print(f"    {c},")
        print('  ],')
print('}')
